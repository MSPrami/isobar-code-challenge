import { combineReducers, createStore, applyMiddleware } from 'redux';
import { logger } from 'redux-logger'

import KenoGameReducer from '../components/Game/KenoGameReducers';

const reducer = combineReducers({
  kenoGame: KenoGameReducer,
});

export default function configureStore(initialState) {
  return createStore(
    reducer,
    initialState,
    applyMiddleware(
      logger,
    ),
  );
}
