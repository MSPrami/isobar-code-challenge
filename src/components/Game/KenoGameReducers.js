import {
  DRAW_BALL,
  GENERATE_TICKET_NUMBER_LIST,
} from './KenoGameActions';

function drawBall(state, action) {
  return {
    ...state,
    drawnBallList: action.drawnBallList,
  };
}

function generateTicketNumberList(state, action) {
  return {
    ...state,
    selectedTicketNumberList: action.selectedTicketNumberList,
  }
}

const initialState = {
  drawnBallList: [],
  selectedTicketNumberList: [],
};

export default function KenoGameReducers(state = initialState, action) {
  switch (action.type) {
    case DRAW_BALL:
      return drawBall(state, action);
    case GENERATE_TICKET_NUMBER_LIST:
      return generateTicketNumberList(state, action);
    default:
      return state;
  }
}