import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import BallDraw from './components/BallDraw/BallDraw';
import CreateTicket from './components/CreateTicket/CreateTicket';

import './KenoGame.scss';

const KenoGame = () => {
  return(
    <Container className="sectionHeading kenoGame">
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <CreateTicket />
          <BallDraw />
        </Col>
      </Row>
    </Container>
  )
}

export default KenoGame;