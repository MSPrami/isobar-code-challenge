import { createSelector } from 'reselect';

const drawnBallListSelector = state => state.kenoGame.drawnBallList;
const selectedTicketNumberListSelector = state => state.kenoGame.selectedTicketNumberList;

export const getDrawnBallList = createSelector(
  [drawnBallListSelector],
  state => state,
);

export const getSelectedTicketNumberList = createSelector(
  [selectedTicketNumberListSelector],
  state => state,
);

