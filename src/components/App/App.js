import React from 'react';

import KenoGame from '../Game/KenoGame';
import './App.scss';

function App () {
  return(
     <div>
       <KenoGame />
     </div>
  );
}
export default App;