 export function sortList(list, order) {
  list.sort((a, b) => {
    return order === 'Asc' ? a - b :  b - a;
  });
  return list;
}