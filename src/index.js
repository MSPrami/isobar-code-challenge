import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './reducers';
import App from './components/App/App';

const store = configureStore();

function render(Component) {
  ReactDOM.render(
  <Provider store={store}><Component /></Provider>,
    document.getElementById('root'),
  );
}

render(App);